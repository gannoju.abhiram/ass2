function calculate() {
    var num1 = parseFloat(document.getElementById("num1").value);
    var operator = document.getElementById("operator").value;
    var num2 = parseFloat(document.getElementById("num2").value);

    var result;
    switch (operator) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            if (num2 !== 0) {
                result = num1 / num2;
            } else {
                alert("Division by zero is undefined.");
                return;
            }
            break;
        default:
            alert("Invalid operator");
            return;
    }
    
    document.getElementById("result").innerHTML = "Result: " + result;
}